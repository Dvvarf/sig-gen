<?php

class myshows_mod {

function GetData($config,$get,$post) {
	// urls
	// we got no check for this url at all, so be careful with it!
	// check it by yourself in your browser if not sure!
	$profile = 'http://api.myshows.ru/profile/'.$config['myshows_login'];

	// using connector to fetch json data
	include_once('./connector.php');
	$conn = new connector();
	$json = $conn->fetchURLdata($profile);
	if($json == false) return 'Connection failed';

	// parsing json data
	if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
		$profile = json_decode($json, true, 4);
	} else {
		$profile = json_decode($json, true);
	}

	// saving a few kB of memory
	unset($profile['friends']); unset($profile['followers']);

	// and finally - parsing
	return $config['myshows_login'] .' потратил '. $profile['wastedTime'] .' '. $this->declOfNum($profile['wastedTime'], array('час','часа','часов')) .' на сериалы';
}

function declOfNum($number, $titles) {
	$cases = array (2, 0, 1, 1, 1, 2);
	return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
}

}

?>
<?php
/*
 * MAL list parsing module for UB generator by Dvvarf
 */

class mal_mod {
	var $TextNo 	= 0;
	var $LayerNo	= 0;
	var $Data		= null;

/*
 * Default module GetData function
 * Recieves all needed data (configuration and additional parameters), fetches all data from configured MAL profile, parses it and output formatted string with requsted data
 *
 * @param	array	$config		full configuration array from parent (single-line.php)
 * @param	array	$get		GET data from parent (single-line.php)
 * @param	array	$post		POST data from parent (single-line.php)
 *
 * @return	string				formatted string with requested info described in $config
 *
 */

function GetData($config, $get, $post) {
	include_once('./connector.php');
	$dplace = new Connector();

	// protection file
	$prot_file = $config['MAL_protection'];

	// if we are going to use this module more than once - we'll try to cache all info in memory
	$all = ($config['mod_usage']['mal_mod'] > 1) ? true : false;

	// trying to get data from MAL
	$rdata = $dplace->fetchURLdata('http://myanimelist.net/rss.php?type=rw&u='.$config['MAL_user'],true,true); // fetching the data

	// nothing recieved? trying to load data from cache
	if($rdata == false)
		$rdata = $this->CacheCheck($config, $prot_file);

	if(!$rdata) {
		// cache is empty? returning an error
		return ($config['textOverError'] === false) ? 'Connection error' : $config['textOverError'];
	} else {
		// we got data from MAL directly or from cache, we can now proceed to parsing
		include_once('./text_func.php');

		$this->Data = $this->PrepareTheData($rdata, $all);
		$prot = true;

		if(!$this->Data)
			$prot = $this->CacheCheck($config,$prot_file);

		if(!$prot) {
			// reading from cache was not successfull, display an error
			return ($config['textOverError'] === false) ? 'Error downloading RSS' : $config['textOverError'];
		} elseif($prot === true) {
			// cache is expired and new rss data was recieved succesfully - rewriting protection file
			$lm = (file_exists("$pth")) ? filemtime($pth) : 0;
			if(((time() - $lm) > 86400) || (sprintf("%u", filesize($file)) < 2)) $dplace->writeCachedResult($prot_file);
		} else {
			// mal cannot be reached - parsing info from protection file
			$this->Data = $this->PrepareTheData($prot, $all);
		}

		// here we'll choose one and only string with data
		$final = ($all) ? $this->Data[$this->TextNo] : $this->Data;

		if(isset($config['smart_trim']) && ($config['smart_trim'] == false)) {
			// if smart_trim is diabled for some reason
			if((isset($config['ptw_template'])) && strtolower(strtolower($final['status'])) == 'plan to watch') {
				$text = textlimit(ParseTemplate($config['ptw_template'], $final), $config['limit']);
			} else {
				$text = textlimit(ParseTemplate($config['text'], $final), $config['limit']);
			}
		} else {
			// lets smart trim the final string
			if((isset($config['ptw_template'])) && strtolower(strtolower($final['status'])) == 'plan to watch') {
				$totlen = $config['limit'] - (my_strlen(ParseTemplate($config['ptw_template'], $final), $config['english_only']) - my_strlen($final['title'], $config['english_only']));
				$final['title'] = textlimit($final['title'], $totlen);
				$text = ParseTemplate($config['ptw_template'], $final);
			} else {
				$totlen = $config['limit'] - (my_strlen(ParseTemplate($config['text'], $final), $config['english_only']) - my_strlen($final['title'], $config['english_only']));
				$final['title'] = textlimit($final['title'], $totlen);
				$text = ParseTemplate($config['text'], $final);
			}
		}

		return $text;
	}
}

/*
 * Parses MAL xml, removing all whitespace characters
 *
 * @param	array	$data		XML string to parse
 * @param	boolean	$all		true if output should be an multi-dimensional array with all titles
 *
 * @return	array				array, containing parsed data
 * @return	boolean				false if parsing returned empty results
 *
 */

function PrepareTheData($data, $all = false) {
	$buffer = strtr($data, array("\n" => '', "\r" => '', "\t" => '', '&lt;' => '<', '&gt;'=>'>', '&amp;' => '&', '&quot;' => '"', '&apos;'=>"'") );
	// these lines just extract the anime title and status information into $titles[] and $status[] arrays, plus other info
	preg_match_all("/<item><title>([^<]*)<\/title>/i", $buffer, $titlematches);
	preg_match_all("/<description>([^<]*) - ([\d?]+) of ([\d?]+) (episodes?|chapters?)<\/description>/i", $buffer, $statusmatches);
	preg_match_all("/<link>http:\/\/myanimelist.net\/anime\/([\d?]+)\/([^<]*)<\/link>/i", $buffer, $idmatches);

	$titles = $titlematches[1];		// $titles is now an array of titles
	$status = $statusmatches[1];	// $status is now an array of statuses
	$current = $statusmatches[2];	// $current is now an array of all the current episodes
	$totals = $statusmatches[3];	// $totals is now an array of all the episode totals
	$ids = $idmatches[1];			// $ids now contains MAL-IDs of titles

	if($titles[0]=='' || $status[0]=='' || $current[0]=='') return false;
	$output = array();

	$k = ($all) ? count($titles) : 1;
	for($i=0; $i<$k; $i++) {
		$output[$i]['title']  = $titles[$i];
		$output[$i]['status'] = $status[$i];
		$output[$i]['ep']     = $current[$i];
		$output[$i]['total']  = $totals[$i];
		$output[$i]['id']     = $ids[$i];
	}

	return ($all) ? $output : $output[0];
}

/*
 * Checks some file for existence and, if exists and opened succesfully - returns its contents
 *
 * @param	array	$config		XML string to parse
 * @param	string	$path		path to file to check
 *
 * @return	string				$path file contents
 * @return	boolean				false if file does not exist or not reachable
 *
 */

function CacheCheck($config, $path) {
	if(($path) && (file_exists($path)) && (sprintf("%u", filesize($path)) > 2)) {
		$handle = fopen($path, 'r');
		$xmlstr = fread($handle, filesize($path));
		fclose($handle);
		return ($xmlstr) ? $xmlstr : false;
	} else return false;
}

}

?>

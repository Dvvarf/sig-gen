<?php
/*
	Graphics class for UserBar Generator by Dvvarf
	Varsion: 0.2
*/
ini_set('display_errors', '1'); ini_set('error_reporting', E_ALL);
include_once('./text_func.php');

class ubGraphics {

var $tcolor = 'fff5ff';
var $config = array();

/**
* Class constructor
*
* Sets $config to use in class
*
* @param	string	$ubar		$ubar from single-line.php aka configuration
* @return						constructor should return nothing, right?
*
*/

function ubGraphics($ubar) {
	$this->config = $ubar;
}

/**
* Create Background function
*
* Triggered when 'bg' layer processing, this function will try to create background from image with filename from
* $config['base'] and if it does not exist - try to fetch 'default.png'. If even that one is unavailable,
* then function will create gradiented background.
*
* @param	string	$width				width for background to be created
* @param	string	$height				same as width, but height
* @param	boolean	$stretch			are we going to resize image if it is not have the size of $width x $height?
* @param	boolean	$preserve_aspect	are we going to preserve its aspect ratio if we stretching it?
* @return								returns image resource with background
*
*/

function CreateBG($width=350,$height=19,$stretch = false,$preserve_aspect = false) {
	if(((bool)($this->config['base']))&&(!(bool)$this->config['grad'])) {
		if(!file_exists('bg/'.$this->config['base'])){
			if(!file_exists('bg/default.png'))
				return $this->CreateGradientLayer($this->config['width'],$this->config['height'],$this->config['gradStart'],$this->config['gradEnd'],$this->config['gradVert']);
		} else {
			$image = $this->CreateBGfromImage($this->config['base'],$this->config['width'],$this->config['height'],true);
		}
		
		if($image) {
			return $image;
		} else {
			return $this->CreateGradientLayer($this->config['width'],$this->config['height'],$this->config['gradStart'],$this->config['gradEnd'],$this->config['gradVert']);
		}
	} else {
		return $this->CreateGradientLayer($this->config['width'],$this->config['height'],$this->config['gradStart'],$this->config['gradEnd'],$this->config['gradVert']);
	}
}

/**
* Create Background from Image
*
* This function is called from CreateBG when there is image which is going to be background
*
* @param	string	$filename			filename of the image becoming background
* @param	string	$width				width for background to be created
* @param	string	$height				same as width, but height
* @param	boolean	$stretch			are we going to resize image if it is not have the size of $width x $height?
* @param	boolean	$preserve_aspect	are we going to preserve its aspect ratio if we stretching it?
* @return								returns image resource with background
*
*/

function CreateBGfromImage($filename,$width=350,$height=19,$stretch = false,$preserve_aspect = false) {
	// creating bg from file with $filename

	$fileformat = substr($filename, strrpos($filename, '.')+1);
	switch ($fileformat) {
		case 'png':		$src = imagecreatefrompng('bg/'.$filename); break;
		case 'jpg':		$src = imagecreatefromjpeg('bg/'.$filename); break;
		case 'jpeg':	$src = imagecreatefromjpeg('bg/'.$filename); break;
		case 'gif':		$src = imagecreatefromgif('bg/'.$filename); break;
		default: return false;
	}
	$sourcew = imagesx($src); $sourceh = imagesy($src);
	if(($width==$sourcew) && ($height==$sourceh)) { $stretch = false; $preserve_aspect = false; }
	
	if((!$stretch) && (!$preserve_aspect)) {
		if($fileformat=='png') {
			$dst = $this->CreateTransparentImage($sourcew,$sourceh);
			imagealphablending($src, true);
			imagecopy($dst, $src, 0, 0, 0, 0, $sourcew, $sourceh);
			imagedestroy($src);
			return $dst;
		} else {
			return $src;
		}
	} elseif($stretch && (!$preserve_aspect)) {
		if($fileformat=='png') {
			$dst = $this->CreateTransparentImage($sourcew,$sourceh);
			imagealphablending($src, true);
			imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $sourcew, $sourceh);
			imagedestroy($src);
			return $dst;
		} else {
			return $src;
		}
	} else {
		if($fileformat=='png') {
			$dst = $this->CreateTransparentImage($sourcew,$sourceh);
			imagealphablending($src, true);
			imagedestroy($src);
		}
		$sourcew = imagesx($src); $sourceh = imagesy($src);
		if($sourcew > $sourceh) {
			$destw = $width;
			$desth = ($width / $sourcew)*$sourceh;
		} else {
			$desth = $height;
			$destw = ($height / $sourceh)*$sourcew;
		}
		$dst = $this->CreateTransparentImage($width,$height);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $destw, $desth, $sourcew, $sourceh);
		return $dst;
	}
}

/**
* Creates gradiented layer
*
* This function is called from CreateBG when the gradient should be created in background
*
* @param	string	$width				width for background to be created
* @param	string	$height				same as width, but height
* @param	string	$base				color to start from (left/top one) in hex format like 'ffffff'
* @param	string	$end				color to end with (right/bottom one) in hex format like '000000'
* @param	boolean	$vertical			gradient should be vertical (true) or horizontal (false, default)
* @param	integer	$step				step to change color (like 0 for 'once on every pixel') - performance option
* @param	boolean	$transparent		do we need second color to be transparent? (deprecated)
* @param	integer	$basewidth			x coord to start from
* @param	integer	$baseheight			y coord to start from
* @return								returns gradiented image resource
*
*/

function CreateGradientLayer($width=350,$height=19,$base='ffffff',$end='000000',$vertical=false,$step=0,$transparent=true,$basewidth=true,$baseheight=true) {
	// creates and returns gradiented layer
	if($basewidth===true) $basewidth = $width;
	if($baseheight===true) $baseheight = $height;
	$im = $this->CreateTransparentImage($width,$height);
	// Break colours into RGB components
	sscanf($base, "%2x%2x%2x", $rbase, $gbase, $bbase);
	sscanf($end, "%2x%2x%2x", $rend, $gend, $bend);
	// Set the Variable to step to use height
	$varstep=($vertical)?$height:$width;
	// Remove potential divide by 0 errors.
	if ($rbase == $rend)
		$rend = $rend -1;
	if ($gbase == $gend)
		$gend = $gend -1;
	if ($bbase == $bend)
		$bend = $bend -1;
	// Make sure the height is at least 1 pixel
	if ($varstep == 0)
		$varstep=1;
	// Set up step modifiers for each colour
	$rmod = ($rend - $rbase) / $varstep;
	$gmod = ($gend - $gbase) / $varstep;
	$bmod = ($bend - $bbase) / $varstep;
	
	// Loop for the height at a rate equal to the steps.
	for($i = 1; $i < $varstep; $i = $i + $step + 1) {
		//Adjust the colours
		$clour1 = ($i * $rmod) + $rbase;
		$clour2 = ($i * $gmod) + $gbase;
		$clour3 = ($i * $bmod) + $bbase;
		$col = imagecolorallocate($im, $clour1, $clour2, $clour3);
		//Paint the rectangle at current colour.
		if ($vertical) {
			imagefilledrectangle($im, $width - $basewidth, $i - $baseheight, $basewidth, $baseheight - $i + $step, $col);
		} else {
			imagefilledrectangle($im, $i - $basewidth, $height - $baseheight, $basewidth - $i + $step, $baseheight, $col);
		}
	}
	return $im;
}

/**
* Mashes layers from image resources on the image
*
* This function mashes $layer image on $img image
*
* @param	image resource			$img				image resource on which the $layer should be mashed
* @param	image resource			$layer				$layer to mash on $img
* @param	boolean					$created			should be set to true if layer was created inside ubGraphics (also optimization)
* @return	boolean										returns true if completed successfully and false if $layer is not an image resource or something has gone wrong
*
*/

function MashLayer($img, $layer, $created = true) {
	// mashing $layer (only one and it should be image object) to $img respecting position set in $config
	if(is_resource($layer)) {
//		imagealphablending($layer,true);
		// copy $layer on $image keeping transparency in mind
		return ($created) ? imagecopymerge_a($img, $layer, 0, 0, 0, 0, $this->config['width'], $this->config['height'], 100) :
							imagecopymerge($img, $layer, 0, 0, 0, 0, $this->config['width'], $this->config['height'], 100);
	} else
		return $img;
}

/**
* Mashes layers from files on the image
*
* This function loads $layer image file and mashes it on $img image
*
* @param	image resource			$img				image resource on which the $layers should be mashed
* @param	array/string			$layers				layer to mash (string with filenames with layers in .png)
* @return	boolean										returns true if completed successfully and false if $layer is not a string or file with this filename does not exist
*
*/

function MashLayerFF($img, $layer) {
	if (is_string($layer) && file_exists($layer)) {
		$imgTmp = imagecreatefrompng($layer);
		imagealphablending($imgTmp, true);
		imagecopy($img, $imgTmp, 0, 0, 0, 0, imagesx($imgTmp), imagesy($imgTmp));
		imagedestroy($imgTmp);
		return $img;
	} else
		return $img;
}

/**
* Mashes layers (defined by filename or image resource) on the image
*
* This function grabs all the image files defined in $layers and mashes it on the image
*
* @param	image resource			$img				image resource on which the $layers should be mashed
* @param	array/string			$layers				layers to mash (array or single element containing filenames of layers or with image resources)
* @return	boolean										returns true if completed successfully and false if $layers is not array or string
*
*/

function MashLayers($img, $layers, $created = true) {
//	imagealphablending($img, true);
	if(is_array($layers)) {
		foreach($layers as $layer)
			if (is_string($layer)) {
				$this->MashLayerFF($img, $layer);
			} else {
				$this->MashLayer($img, $layer, $created);
			}
	} elseif(is_string($layers)) {
		$this->MashLayerFF($img, $layers);
	} elseif(is_resource($layers)) {
		$this->MashLayer($img, $layers, $created);
	}
	
	return $img;
}

function CreateTextRefLayer($text, $align_x='r7', $align_y='c') {
	$img = $this->CreateTransparentImage($this->config['width'], $this->config['height']);
	return $this->CreateTextAndReflection($img, $text, $align_x, $align_y);
}

function CreateTextAndReflection($image, $text, $align_x='r7', $align_y='c', $baseline = true) {
	// mashes text on $image with reflection
	$img = $this->CreateText($text,&$fin);
	
	// text params
	$border = $this->config['fontBorder'];
	$widthb  = $fin['w'];
	$heightb = $fin['h'];
	$heightab = $fin['ab'];
	
	// text positions on $image
	$getposx = $this->config['text_x'] + GetPosX($widthb,$align_x,$this->config['text_width']);
	$getposy = $this->config['text_y'] + GetPosY($heightab,$align_y,$this->config['text_height']);
	$getposy_ref = $this->config['text_y'] + GetPosY($heightb,$align_y,$this->config['text_height']);
	
	imagealphablending($img,true);
	imagecopy($image, $img, $getposx, $getposy, 0, 0, $widthb, $heightb);
//	imagecopymerge_a($image, $img, $this->config['text_x'] + GetPosX($widthb,$align_x,$this->config['text_width']), $this->config['text_y'] + GetPosY($heightb,$align_y,$this->config['text_height']), 0, 0, $fin['w'], $heightb, 100);
	$alf_end = $this->config['alpha_end'];
	$alf_start = $alf = $this->config['alpha_start'];
	$alf_step = abs($alf_end - $alf_start)/$heightb;
	$prcnt = 1 - $this->config['prcnt']/100;
	
//	imagealphablending($img,false);
	for ($y = $heightb*$prcnt; $y < $heightb; $y++) {
		$alf = $alf + $alf_step;
		if ($alf > 100) $alf = 100;
		imagecopymerge_a($image, $img, $getposx, $getposy_ref + $heightb*2 - $y, 0, $y, $widthb, 1, $alf);
	}
	
	return $image;
}

function MashTextLayer($image, $text, $align_x='r7', $align_y='c') {
	if($this->config['text_width'] == 0) $this->config['text_width'] = $this->config['width'];
	if($this->config['text_height'] == 0) $this->config['text_height'] = $this->config['height'];

	return $this->MashTextLayerCustom( $image, $text, $align_x, $align_y, $this->config['text_width'], $this->config['text_height'], $this->config['text_x'], $this->config['text_y']);
}

function MashTextLayerCustom($image, $text, $align_x = 'r7',$align_y = 'c', $tw = 0, $th = 0, $tx = 0, $ty = 0) {
	if($tw == 0 || $th == 0) return $this->CreateTransparentImage(1,1); // safe from returning boolean and all the consequences
	
	// create end mash layer with text
//	$image = $this->CreateTransparentImage($w,$h);
	$img = $this->CreateText($text,&$fin);

	// lets mash our text layer with final layer
	$border  = $this->config['fontBorder'];
	$widthb  = $fin['w'] + $border*2;
	$heightb = $fin['h'] + $border*2;
	//var_dump($heightb); var_dump($widthb); die();
	
	imagealphablending($img,true);
	imagecopymerge_a($image, $img, $tx + GetPosX($widthb,$align_x,$tw), $ty + GetPosY($heightb,$align_y,$th), 0, 0, $widthb, $heightb, 100);
	return $image;
}

function CreateTextLayer($text,$align_x='r7',$align_y='c') {
	if($this->config['text_width'] == 0) $this->config['text_width'] = $this->config['width'];
	if($this->config['text_height'] == 0) $this->config['text_height'] = $this->config['height'];

	return $this->CreateTextLayerCustom( $text, $align_x, $align_y, $this->config['width'], $this->config['height'], $this->config['text_width'], $this->config['text_height'], $this->config['text_x'], $this->config['text_y'] );
}

function CreateTextLayerCustom($text, $align_x = 'r7', $align_y = 'c', $w = 0, $h = 0, $tw = 0, $th = 0, $tx = 0, $ty = 0) {
	if($tw == 0 || $th == 0) return $this->CreateTransparentImage(1,1); // safe from returning boolean and all the consequences
	//if($tw == 0) $tw = $w;
	//if($th == 0) $th = $h;
	
	// create end mash layer with text
	$image = $this->CreateTransparentImage($w,$h);
	$img = $this->CreateText($text,&$fin);

	// lets mash our text layer with final layer
	$border  = $this->config['fontBorder'];
	$widthb  = $fin['w'];
	$heightb = $fin['h'];
	$heightab = $fin['ab'];
	
	imagealphablending($img,true);
	imagecopymerge_a($image, $img, $tx + GetPosX($widthb,$align_x,$tw), $ty + GetPosY($heightab,$align_y,$th), 0, 0, $widthb, $heightb, 100);
	//imagecopymerge_a($image, $img, $tx + GetPosX($widthb,$align_x,$tw), $ty + GetPosY($heightb,$align_y,$th), 0, 0, $fin['w']*2, $heightb, 100);
	return $image;
}

function CreateText($text, $finale = array(), $size_over = false) {
	// returns image resource with $text on white background
	$ifontSize = ($size_over === false)?$this->config['fontSize']:$size_over;
	$font = (isset($this->config['fontdir']))?"{$this->config['fontdir']}{$this->config['font']}":"./fonts/{$this->config['font']}";
	
	if(!isset($this->config['hfix'])) $this->config['hfix'] = 0;
	$angle = 0;
	
	// then just pass all the values to the CreateTextCustom function
	return $this->CreateTextCustom($text, $font, $this->config['fontSize'], $this->config['fontColor'], $this->config['fontBorderColor'], $angle, &$finale, $this->config['fontBorder'], $this->config['kerning'], $this->config['fontSizepx'], $this->config['hfix']);
}

function CreateTextCustom($text, $font, $ifontSize, $ifontColor, $ifontBorderColor, $iAngle, $finale = array(), $fontBorder = 0, $ikerning = 0, $px = false, $hfix = 0) {
	// if px is true will try to calculate right size from value in pixels (all these GD1/GD2 sizes and other problems)
	if($px) {
		$i = 1; $h = 1;
		while(( $h < $ifontSize) && ($i<100)) {
			$i++;
			$bbox = imagettfbbox($i, $iAngle, $font, $text);
			$h = abs($bbox[1] - $bbox[5]);
		}
		$fontSize = $i;
	} else $fontSize = $ifontSize;
	
	// creating boundary boxes to test sizes
	$border = $fontBorder;

	// imagettfbbox hint:
	// (6,7)-----(4,5)
	//   |         |
	//   |         |
	// (0,1)-----(2,3)

	$bbox = imagettfbboxf($fontSize, $iAngle, $font, $text);
	// width of the resulting text box:
	$wi = max($bbox[0], $bbox[2], $bbox[4], $bbox[6]) - min($bbox[0], $bbox[2], $bbox[4], $bbox[6]) + $border*2;
	// height of the resulting text box:
	$fh = max($bbox[1], $bbox[3], $bbox[5], $bbox[7]) - min($bbox[1], $bbox[3], $bbox[5], $bbox[7]) + $border*2;
	//$wi = distance($bbox[6], $bbox[7], $bbox[4], $bbox[5]) + $border*2; // width of the resulting text
	//$fh = distance($bbox[0], $bbox[1], $bbox[6], $bbox[7]) + $border*2; // height of the resulting text
	
	// baseline calculations:
	if($iAngle == 0) {
		$ab = distance(0, 0, $bbox[6], $bbox[7]) + $border; // height of the text above the baseline (ab = above baseline)
		$bb = distance($bbox[0], $bbox[1], 0, 0) + $border; // height of the text below the baseline (bb = below baseline)
	} else {
		$ab = (distance(0, 0, $bbox[6], $bbox[7]) + $border)*sin( deg2rad($iAngle) ); // height of the text above the baseline (ab = above baseline)
		$bb = (distance($bbox[0], $bbox[1], 0, 0) + $border)*sin( deg2rad($iAngle) ); // height of the text below the baseline (bb = below baseline)
	}

	// max height of letters on the resulting image:
	$bbox_he = imagettfbboxf($fontSize, $iAngle, $font, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz');
	$he = distance($bbox_he[0], $bbox_he[1], $bbox_he[6], $bbox_he[7]) + $border*2;

	// fixing starting positions for custom angles:
	$hpos = $border;
	if($iAngle != 0) $hpos += abs($he) * sin( deg2rad($iAngle) ) - $bbox_he[0] - $border; // $bbox_he[0] - x coord under baseline
	$vpos = $fh - $bbox[1] - $border*2; // $bbox_he[y] - y coord under baseline, $bbox - applied coord under the baseline

	$im = $this->CreateTransparentImage($wi,$fh);
	
	imagealphablending($im, true);
	$fontColor = $this->hex_imagecolorallocate($im, $ifontColor);
	$fontBorderColor = $this->hex_imagecolorallocate($im, $ifontBorderColor);
	if((bool)$ikerning) {
		// dont forget to use applykerning()
		$text_arr = my_split($text);
		$kerning = $ikerning;
		foreach($text_arr as $single_word) {
			$this->CreateTextStroke($im, $fontSize, $iAngle, $hpos, $he, $fontBorderColor, $font, $single_word, $border);
			$bbox = imagettftext($im, $fontSize, $iAngle, $hpos, $he, $fontColor, $font, $single_word);
			if($single_word==' ') { $hpos = $bbox[2]; } else { $hpos = $bbox[2] + $kerning; }
		}
	} else {
		$this->CreateTextStroke($im, $fontSize, $iAngle, $hpos, $vpos, $fontBorderColor, $font, $text, $border);
		$bbox = imagettftext($im, $fontSize, $iAngle, $hpos, $vpos, $fontColor, $font, $text);
//		$finalh = abs($bbox[1] - $bbox[5]) + $border*2;
//		$finalh = max($bbox[1], $bbox[3], $bbox[5], $bbox[7]) - min($bbox[1], $bbox[3], $bbox[5], $bbox[7]) + $border*2;
		$finalh = distance($bbox[0], $bbox[1], $bbox[6], $bbox[7]) + $border*2;
		$hpos = abs($bbox[6] - $bbox[0]);
	}
	
	//if(!isset($finalh)) $finalh = $fh;
	// lets mash our text layer with final image
	$finale['h'] 	= $fh + $hfix;		// actual height of text box
	//$finale['h2'] = $he + $hfix;		// actual height of text (height of letters) from imagettfbbox
	$finale['ab']	= $ab + $hfix;		// height of letters above the baseline
	$finale['bb']	= $bb + $hfix;		// height of letters below the baseline
	$finale['w'] 	= $wi;			// actual width of text box
	$finale['w2'] 	= $hpos;				// width of text from imagettfbbox + border
	return $im;
}

function CreateTransparentImage($wi = 350, $he = 19) {
	$im = imagecreatetruecolor($wi,$he);
	imagesavealpha($im, true);
	$clr = $this->hex_imagecolorallocatealpha($im, $this->tcolor);
//	old way:
//	imagecolortransparent($im, $clr);
//	imagefilledrectangle($im, 0, 0, $wi, $he, $clr);

//	new (right!) way to do things:
	imagefill( $im, 0, 0, $clr );
	return $im;
}

function CreateTextStroke($image, $fontSize, $textAngle, $textWidth, $textHeight, $borderColor, $fontFile, $text, $borderWidth) {
	if($borderWidth != 0)
		for($x = ($textWidth-$borderWidth); $x <= ($textWidth+$borderWidth); $x++) // x-axis
			for($y = ($textHeight-$borderWidth); $y <= ($textHeight+$borderWidth); $y++) // y-axis
				imagettftext($image, $fontSize, $textAngle, $x, $y, $borderColor, $fontFile, $text);
	return $image;
}

function hex_imagecolorallocate($img,$hexclr) {
	return imagecolorallocate($img, hexdec('0x'.$hexclr{0}.$hexclr{1}), hexdec('0x'.$hexclr{2}.$hexclr{3}), hexdec('0x'.$hexclr{4}.$hexclr{5}));
}

function hex_imagecolorallocatealpha($img,$hexclr,$trnsprt=127) {
	return imagecolorallocatealpha($img, hexdec('0x'.$hexclr{0}.$hexclr{1}), hexdec('0x'.$hexclr{2}.$hexclr{3}), hexdec('0x'.$hexclr{4}.$hexclr{5}), $trnsprt);
}

}

/*
 * Function to correctly merge images with alpha-channel
 * For more information see official documentation on imagecopymerge function:
 *		http://php.net/manual/function.imagecopymerge.php
 *
 * @param	resource	$dst_im		url destination image link resource
 * @param	resource	$src_im		source image link resource
 * @param	int			$dst_x		x-coordinate of destination point
 * @param	int			$dst_y		y-coordinate of destination point
 * @param	int			$src_x		x-coordinate of source point
 * @param	int			$src_y		y-coordinate of source point
 * @param	int			$src_w		source width
 * @param	int			$src_h		source height
 * @param	int			$pct		when pct = 0, no action is taken, when 100 this function implements alpha transparency for true colour images
 *
 * @return	boolean					returns true on success or false on failure
 *
 */

function imagecopymerge_a($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct, $d = false) {
	if($pct == 0) return false; // behaving like official imagecopymerge
	imagesavealpha($dst_im, true);
	
	// discarding $src_w and $src_h if out of bounds using real image's width and height
	$src_wr = imagesx($src_im);
	$src_hr = imagesy($src_im);
	$src_w = ($src_wr < $src_w) ? $src_wr : $src_w;
	$src_h = ($src_hr < $src_h) ? $src_hr : $src_h;
	
	// iterating through every pixel on $src_im
	for($y = 0; $y < $src_h; $y++)
		for($x = 0; $x < $src_w; $x++) {
			// initial values init and checking (optimization)
			$dy = $dst_y + $y;
			if($dy < 0) continue;
			$sy = $src_y + $y;
			if($sy < 0) continue;
			$sx = $src_x + $x;
			if($sx < 0) continue;
			$dx = $dst_x + $x;
			if($dx < 0) continue;
			
			// getting color id on current pixel
			$src_p = imagecolorat($src_im, $sx, $sy);
			
			// fast as lightning parsing of color id $src_p
			$a = ($src_p & 0x7F000000) >> 24;
			if($a == 127) continue; // any $pct will not affect complete transparency
			$r = ($src_p >> 16) & 0xFF;
			$g = ($src_p >> 8) & 0xFF;
			$b = $src_p & 0xFF;

			// calculating new alpha value for pixel to set
			if($pct == 100) {
				$new_a = $a;
			} else {
				$new_a = ( $a == 127 ) ? 127 : $a + ( 127 - $a ) * ( 1 - $pct / 100 ); // golden!
			}
			if($new_a == 127) continue; // why bother setting pixel if it is completely transparent?
			
			// fast method to get color id without color allocation (the same used for color allocation in php sources)
			$new_p = ($new_a << 24) + ($r << 16) + ($g << 8) + $b;
			
			// and finally applying new pixel to $dst_im
			imagesetpixel($dst_im, $dx, $dy, $new_p);
		}
	return true;
}

?>
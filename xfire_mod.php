<?php

class xfire_mod {

function GetData($config,$get,$post) {
	// urls
	$id_url = 'http://www.xfire.com/xml/'.$config['xfire_profile'].'/gameplay/';
	
	// using connector to fetch xml data
	include_once('./connector.php');
	$conn = new connector();
	$pth = './cache/xfire.xml';
	$lm = (file_exists("$pth"))?filemtime($pth):0;
	if((time() - $lm) > 86400) {
		$xmlstr = $conn->fetchURLdata($id_url,$pth);
	} elseif(is_readable("$pth")) {
		$file = fopen("$pth","r");
		$xmlstr = '';
		while ( ($buf=fread( $file, 8192 )) !== '' ) { $xmlstr .= $buf; }
		fclose($file);
	}

	if(!$xmlstr) return 'Connection failed';
	
	// parsing xml while handling all errors
	libxml_use_internal_errors(true);
	$xml_profile = new SimpleXMLElement($xmlstr);
	if(!$xml_profile) {
		return 'Failed to parse the data';
	}
	
	$all = 0;
	foreach($xml_profile->game as $gm) {
		$dt = (string)$gm->shortname[0];
		$gm_arr[$dt] = (string)$gm->alltime;
		// caching...
		$url = 'http://media.xfire.com/xfire/xf/images/icons/'. $gm->shortname .'.gif';
		$pth = './cache/xfire_'. $gm->shortname .'.gif';
		if(!file_exists("$pth")) $conn->fetchURLdata($url,$pth,false);
		$all += $gm->alltime;
	}
	unset($xml_profile); unset($conn);
	
	arsort($gm_arr); //sorting..
	
	$image = imagecreatetruecolor(350,19);
	$clr = $this->hex_imagecolorallocate($image, '000500');
	imagecolortransparent($image, $clr);
	imagefilledrectangle($image, 0, 0, 350, 19, $clr);
	imagealphablending($image,false);
	
	$x = 1; $max = 0;
	foreach($gm_arr as $k => $val) {
		if($x==1) $max = $val;
		$icon = imagecreatefromgif('./cache/xfire_'.$k.'.gif');
		$prcnt = round(($val/$max)*55 + 45);
		imagecopymerge($image,$icon,$x,2,0,0,16,16,$prcnt);
		if($x>350) break;
		$x+=16;
	}
	
	$filename = './bg/xfire_cached.png';
	$success = $this->fwritenclose($filename,$image);
	if($success) $config['overlay'] = 'xfire_cached.png';
	
	return 'Total played '. round($all/3600) .' hrs';
}

function hex_imagecolorallocate($img,$hexclr) {
	return imagecolorallocate($img, hexdec('0x'.$hexclr{0}.$hexclr{1}), hexdec('0x'.$hexclr{2}.$hexclr{3}), hexdec('0x'.$hexclr{4}.$hexclr{5}));
}

function fwritenclose($filename,$a) {
	ob_start();
	imagepng($a);
	$str = ob_get_clean();
	if(!($handle = fopen($filename, 'w')))	return false;
	if(fwrite($handle,$str)===false) 		return false;
	fclose($handle);
	return true;
}

}

?>
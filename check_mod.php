<?php

class check_mod {

function GetData($config,$get,$post) {
	// urls
	$urls = explode('|',$config['check_urls']);
	if(count($urls) < 1) return 'Nothing to check';
	
	// using connector to fetch xml data
	include_once('./connector.php');
	$conn = new connector();
	$url = $urls[array_rand($urls)];
	$status = $conn->checkURLconnect($url);
	$parsed_url = parse_url($url);
	if($status) {
		return $parsed_url['host'] .' is online';
	} else {
		return $parsed_url['host'] .' is offline';
	}
}

}

?>
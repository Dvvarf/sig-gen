<?php
// MAL signature script by Dvvarf

// first off lets copy $_GET and $_POST arrays to local arrays (we can overload them at anytime by doing this)
ini_set('display_errors', '1'); ini_set('error_reporting', E_ALL);
$get = $_GET; $post = $_POST;

// lets handle possible run from console (cron, for example)
if('cli' == PHP_SAPI) {
	$arguments = $_SERVER['argv'];
	foreach($arguments as $arg) {
		if($arg == '-ignore-cache') $get['mode']='ignore_cache';
		elseif(preg_match('/-profile=([^<]*)/i',$arg,$match)>0) $get['profile'] = $match[1];
	//	elseif(preg_match('/-cli_option_name=([^<]*)/i',$arg,$match)>0) $get['option_name_get_proxy'] = $match[1];
	}
	$console_mode = true;
} else {
	$console_mode = false;
}

// lets read config from profile (if set so - read random profile)
if(isset($get['profile'])) {
	$profile = $get['profile'];
	if($profile=='random') {
		$con = folder_content('./profiles/','.prof');
		$profile = $con[array_rand($con)];
	} else $profile .= '.prof';
} else {
	$lock = folder_content('./profiles/','.lock');
	if(isset($lock[0])) {
		$profile = substr($lock[0],0,strrpos($lock[0],'.')).'.prof';
	} else {
		$con = folder_content('./profiles/','.prof');
		$profile = $con[array_rand($con)];
	}
}
include_once('./profiles/'.$profile);
if(!(isset($ubar))) die('default profile was not found');

// its here... temporairly (multi-line.php stuff)
if(!isset($ubar['text_width'])) $ubar['text_width'] = $ubar['width'];
if(!isset($ubar['text_height'])) $ubar['text_height'] = $ubar['height'];
if(!isset($ubar['text_x'])) $ubar['text_x'] = 0;
if(!isset($ubar['text_y'])) $ubar['text_y'] = 0;

// mini settings-manager (helper)
//include_once('./settings-manager.php');

// ignoring cache if needed (also resets cache if it is enabled):
if(isset($get['mode']) && ($get['mode']=='ignore_cache')) {
	$ubar['refresh'] = ($ubar['use_cache'])?true:false;
	$ubar['use_cache'] = false;
}elseif(isset($get['mode'],$ubar['allow_demo']) && ($get['mode']=='demo') && ($ubar['allow_demo'])) {
	$ubar['refresh'] = false;
	$ubar['use_cache'] = false;
} else { $ubar['refresh'] = false; }

// thats all! dont edit below this line until you know what are you doing!
$pth = './'.$ubar['cache_fname'];
$lm = (file_exists("$pth"))?filemtime($pth):0;
if ((!isset($post['user'])) && ($ubar['use_cache']) && (file_exists($pth)) && (abs(time() - $lm)) < $ubar['cache_time']) {
	// loads cache
	header("Content-type: image/png");
	$exp = date("D, d M Y H:i:s T", strtotime('+' . $ubar['cache_time'] . ' seconds', $lm));
	header("Expires: ".$exp);
	header("Last-Modified: ".date("D, d M Y H:i:s T", strtotime('now',$lm)));
	if(is_readable("$pth")) {
		$file = fopen("$pth","r");
		$buffer = '';
		while ( ($buf=fread( $file, 8192 )) !== '' ) { $buffer .= $buf; }
		fclose($file);
		if(!($buffer)) echo '';
		echo $buffer;
	} else {
		$CreateUB = true;
	}
} else {
	$CreateUB = true;
}

if($CreateUB) {
	// let the creation of perfect userbar begin!
	$text_arr = parseTextString($ubar);
	
	$n = count($text_arr['mod']);
	$ubar['mod_usage'] = array_count_values($text_arr['mod']);
	
	for($i = 0; $i < $n; $i++) {
		$mod_set = $text_arr['mod'][$i];
		$ubar['text'] = $text_arr['text'][$i];
		$previous_set = null;
		
		// preparing text entries
		if(($mod_set!='') && ($mod_set!='offline')) {
			include_once('./'.$mod_set.'.php');
			if($mod_set != $previous_set) $mod_obj = new ${"mod_set"}();
			$text_arr['text'][$i] = $mod_obj->GetData(&$ubar, $get, $post);
			if(isset($mod_obj->TextNo)) $mod_obj->TextNo += 1;
			if($console_mode) echo "Got text '{$text_arr['text'][$i]}' from module {$mod_set}.\n";
		} elseif(!isset($ubar['text'])) { // for whatever reason
			$text_arr['text'][$i] = 'It works!';
			$text_arr['ref'][$i] = false;
		}
		$previous_set = $mod_set;
	}

	// loading ubGraphics
	include_once('./graph.php');
	$imgobj = new ubGraphics($ubar);
	
	// preparing layers...
	if(!isset($ubar['layers_order'])) $ubar['layers_order'] = 'bg,scanlines,overlay,text,shine,border'; // default one
	$layers_o = explode(',',$ubar['layers_order']);
	if(!in_array('bg',$layers_o)) array_unshift($layers_o,'bg'); // bg!
	
	// parsing $layers_o and line up all the layers to mash them later
	foreach($layers_o as $layer) {
		//$layer_params = explode(':', $layer, 3);
		// now, presumably, [0] will contain name, [1] - align by x, [2] - align by y
		
		switch($layer) {
			case 'bg':
				$image = $imgobj->CreateBG($ubar['width'], $ubar['height'], true);
				if($console_mode) echo "Background created.\n";
				break;
			case 'text':
				foreach($text_arr['text'] as $k => $text) {
					if($text_arr['ref'][$k]) {
						$ubar['layers'][] = $imgobj->CreateTextRefLayer($text, $text_arr['align_x'][$k], $text_arr['align_y'][$k]);
					} else {
						$ubar['layers'][] = $imgobj->CreateTextLayer($text, $text_arr['align_x'][$k], $text_arr['align_y'][$k]);
					}
					if($console_mode) echo "'{$text}' text layer added.\n";
				}
				if($console_mode) echo "All text layers added.\n";
				break;
			case 'custom':
				if((isset($mod_set)) && ($mod_set!='offline') && (method_exists($mod_obj,'GetLayer'))) {
					$ubar['layers'][] = $mod_obj->GetLayer(&$ubar,$get,$post);
					if(isset($mod_obj->LayerNo)) $mod_obj->LayerNo += 1;
				}
				if($console_mode) echo "Custom layer added.\n";
				break;
			default:
				if(isset($ubar['layer-'.$layer])) {
					$ubar['layers'][] = $ubar['layer-'.$layer];
					if($console_mode) echo "'{$layer}' layer added.\n";
				}
		}
	}
	// mashing all the layers left after text layer
	if(isset($ubar['layers'][0]))
		$imgobj->MashLayers($image, $ubar['layers']);

	unset($imgobj);
	
	// and finally lets show image to user:
	if(!$console_mode) header("Content-type: image/png");
	
	if ($ubar['use_cache'] || $ubar['refresh']) {
		if($console_mode) {
			var_dump($image);
			$result = imagepng($image, $pth);
			echo ($result) ? "Cache image written succesfully.\n" : "Failed to write cache image.\n";
		} else {
			// only output if we are not in console mode
			header("Expires: ".date("D, d M Y H:i:s T", strtotime('+' . $ubar['cache_time'] . ' seconds')));
			header("Last-Modified: ".date("D, d M Y H:i:s T", strtotime('now')));
			imagepng($image);
		}
	} elseif(!$console_mode) { // only output if we are not in console mode
		header("Cache-Control: no-cache, must-revalidate");
		imagepng($image);
	}
	imagedestroy($image);
}

function parseTextString($ubar) {
	$i = (isset($ubar["textString"]))?'':1;
	
	while(isset($ubar["textString{$i}"])) { // while textstring with this number exists
		$current = explode(';;',$ubar["textString{$i}"]);
		$tstr_args = count($current);
		switch($tstr_args) {
			default:
			case 1:
				$text_arr['text'][] = $current[0];
				$text_arr['mod'][] = 'offline';
				break;
			case 3:
				if($current[2] === 'true') {
					$text_arr['ref'][] = true;
				} elseif($current[2] === 'false') {
					$text_arr['ref'][] = false;
				}
			case 2:
				$text_arr['mod'][] = $current[0];
				$text_arr['text'][] = $current[1]; // maybe we should check is it empty and do something about it?
				break;
			case 5:
				if($current[4] === 'true') {
					$text_arr['ref'][] = true;
				} elseif($current[4] === 'false') {
					$text_arr['ref'][] = false;
				}
			case 4: 
				$text_arr['mod'][] = $current[0];
				$text_arr['text'][] = $current[1];
				$text_arr['align_x'][] = $current[2];
				$text_arr['align_y'][] = $current[3];
				break;
		}

		$i = ($i == '')?2:$i+1;
		
		// filling variables left unset with default values
		$j = $i-2;
		if(!isset($text_arr['mod'][$j])) $text_arr['mod'][$j] = 'offline';
		if(!isset($text_arr['text'][$j])) $text_arr['text'][$j] = '...';
		if(!isset($text_arr['align_x'][$j])) $text_arr['align_x'][$j] = 'r7';
		if(!isset($text_arr['align_y'][$j])) $text_arr['align_y'][$j] = 'c';
		if(!isset($text_arr['ref'][$j])) $text_arr['ref'][$j] = $ubar['reflection'];
	}
	
	return $text_arr;
}

// function returns contents of specified folder as array with extension filter
function folder_content($dir_path = './', $filter = '.php', $clean = false) {
	$dir = @opendir($dir_path);
	$content = array();
	while($read = readdir($dir)) if(strripos($read, $filter) !== false) $content[] = $read;
	if($clean) foreach($content as $key => $file) $content[$key] = substr($file, 0, strrpos($file, '.'));
	closedir($dir);
	return $content;
}
?>
<?php
// WoW Armory info fetcing module for Signature Generator by Dvvarf v0.1alpha
// requires SimpleXML component for PHP5+ enabled, no option for PHP4

class wow_mod {

function GetData($config,$get,$post) {
$char = urlencode($config['wow_char']);
$realm = urlencode($config['wow_realm']);
$final_url = 'http://eu.wowarmory.com/character-sheet.xml?r='.$realm.'&n='.$char;
$t1 = microtime();

include_once('./connector.php');
$conn = new connector();
if((int)$config['wow_cache']>0) {
	$pth = './wow_cache.xml';
	$lm = (file_exists("$pth"))?filemtime($pth):0;

	if ((!file_exists($pth)) or ((time() - $lm) > $config['wow_cache'])) {
		$xmlstr = $conn->fetchURLdata($final_url,$pth);
	} else {
		$handle = fopen($pth, 'r');
		$xmlstr = fread($handle, filesize($pth));
		fclose($handle);
	}
} else $xmlstr = $conn->fetchURLdata($final_url);

$t2 = microtime();
$itog = abs($t2-$t1)*1000;
//echo $itog;
$xml = new SimpleXMLElement($xmlstr);

//echo 'Чар с именем '.$xml->characterInfo->character['name'].' ('.$xml->characterInfo->character['class'].'-'.$xml->characterInfo->character['gender'].' '.$xml->characterInfo->character['level'].'-го уровня) живёт в мире '.$xml->characterInfo->character['realm'].' (боевая группа '.$xml->characterInfo->character['battleGroup'].') и у него шанс критического удара = '.$xml->characterInfo->characterTab->melee->critChance['percent'].'%';
//echo "\n (время, которое занял данный запрос - ". $itog*1000 ."мс)";

// syntax example:
unset($a); $a = array(); // lets unset and recreate our array with text to prevent texts from intersection
$a[1]['align'] = 'right';
//$a[1]['text'] = '%name% умеет профессию %prof%';
$a[1]['text'] = 'Чар с именем %name% (%class% %race%-%gender% %level%-го уровня) живёт в мире %realm% (боевая группа %battleGroup%) и у него шанс критического удара = %critChance%%, а силищи в нём аж %strength%';
//$a[2]['align'] = 'left';
//$a[2]['text'] = '%name%';

$custom_text = false; // if this set to true then $a array will be used to generate text, otherwise default texts will be used

$t1 = microtime();
// allowed tags (please, dont select those you dont use - thats increases server load)
$massiv_gde_hranyatsya_chto_parsit['prof'] = true;
$massiv_gde_hranyatsya_chto_parsit['name'] = true;
$massiv_gde_hranyatsya_chto_parsit['race'] = true;
$massiv_gde_hranyatsya_chto_parsit['gender'] = true;
$massiv_gde_hranyatsya_chto_parsit['realm'] = true;
$massiv_gde_hranyatsya_chto_parsit['level'] = true;
$massiv_gde_hranyatsya_chto_parsit['name'] = true;
$massiv_gde_hranyatsya_chto_parsit['class'] = true;
$massiv_gde_hranyatsya_chto_parsit['strength'] = true;
$massiv_gde_hranyatsya_chto_parsit['critChance'] = true;

//у нас задан массив $massiv_gde_hranyatsya_chto_parsit['prof'] и т.д.
//до этого массив $massiv_term не существует (ну или задан как $massiv_term = array()) */
$massiv_term = array();
foreach($massiv_gde_hranyatsya_chto_parsit as $k => $val) {
//	if($val) $massiv_term[$k]['val'] = $xml->chto-to;
	if($val===true) switch ($k) {
//		case 'what_to_parse (from bool array)': $massiv_term['tag_used_in_%%'] => $xml->bububu->char['stat_name']
		case 'level': $massiv_term[$k]['tit'] = 'Уровень: '; $massiv_term[$k]['val'] = $xml->characterInfo->character['level']; break;
		case 'class': $massiv_term[$k]['tit'] = 'Класс: '; $massiv_term[$k]['val'] = $xml->characterInfo->character['class']; break;
		case 'gender': $massiv_term[$k]['val'] = $xml->characterInfo->character['gender']; break;
		case 'race': $massiv_term[$k]['val'] = $xml->characterInfo->character['race']; break;
		case 'realm': $massiv_term[$k]['val'] = $xml->characterInfo->character['realm']; break;
		case 'name': $massiv_term[$k]['tit'] = 'Имя: '; $massiv_term['name']['val'] = $xml->characterInfo->character['name']; break;
		case 'guildName': $massiv_term[$k]['val'] = $xml->characterInfo->character['guildName']; break;
		case 'prof': 
			$temp_prof = $xml->characterInfo->characterTab->professions;
//			var_dump($temp_prof->skill);
			$prof_arr[0] = $temp_prof->skill[0];
			$prof_arr[1] = $temp_prof->skill[1];
			$temp_prof_k = array_rand($prof_arr);
			$massiv_term[$k]['val'] = $prof_arr[$temp_prof_k]['name'].': '.$prof_arr[$temp_prof_k]['value'];
			break;
		case 'health': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->characterBars->health['effective']; break;
		case 'merp': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->characterBars->SecondBar['effective']; break;
		case 'strength': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->baseStats->strength['effective']; break;
		case 'agility': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->baseStats->agility['effective']; break;
		case 'stamina': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->baseStats->stamina['effective']; break;
		case 'intellect': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->baseStats->intellect['effective']; break;
		case 'spirit': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->baseStats->spirit['effective']; break;
		case 'armor': $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->baseStats->armor['effective']; break;
		case 'critChance': $massiv_term[$k]['tit'] = 'Шанс крита: '; $massiv_term[$k]['val'] = $xml->characterInfo->characterTab->melee->critChance['percent'] .'%'; break;
		default: break;
	}
}

if(!$custom_text) {unset($a); $a = array(); $i=1;}
foreach($massiv_term as $key => $value) {
	if($custom_text) {
		foreach($a as $kk=>$vv) { // замещаем %$key% в поданной строке на $value
	//		echo 'replacing %'.$key.'% with '.$value.' in '. $a[$kk]['text'] ."\n"; // debug string
			$str_to_parse = '%'.$key.'%';
			$a[$kk]['text'] = str_replace($str_to_parse, $value['val'], $a[$kk]['text']);
		}
	} else {
			$a[$i]['text'] = $value['tit'].$value['val'];
			$i++;
//			echo $value;
	}
}
$t2 = microtime();
$itog = abs($t2-$t1)*1000;
// текст выдаётся в виде массива a[nomer]['text'] и a[nomer]['align'] в генератор и там уже парсится в таком формате, соответственно сначала нужно научить генератор парсить массив текста с параметрами
// в начале модуля массив с текстом очищается, чтобы не было пересечения текста или что-нить в этом роде

//foreach($a as $valu) echo $valu['text'];
return $a[array_rand($a)]['text'];
//var_dump($a);
//echo '/'.$itog;
//$text = $a[array_rand($a)]['text'];
//return $text;
}

}
?>
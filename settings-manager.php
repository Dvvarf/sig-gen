<?php
// settings manager helper v0.2

if(!(($ubar['protector']===NULL) or ($ubar['protector']==false) or ($ubar['protector']==''))) {
	if( (!(isset($get['protector'])))or($get['protector']!==$ubar['protector']) ) die('Anauthorized access');
}

$ubar['modules'] = 'offline'; // lets increase the speed
if(isset($get['font'])) $ubar['font'] = $get['font'];
if(isset($get['fontSize'])) $ubar['fontSize'] = $get['fontSize'];
if(isset($get['fontSizepx'])) $ubar['fontSizepx'] = $get['fontSizepx'];
if(isset($get['kerning'])) $ubar['kerning'] = $get['kerning'];

if(isset($get['mode']) and $get['mode']=='manager-test') {
	// generating demo image
	$ubar['use_cache'] = false; $ubar['refresh'] = true;
} elseif(isset($get['mode']) and $get['mode']=='manager') {
	// generating page with all the settings and stuff

	// image url generation:
	$f = $ubar['font'];
	$fs = $ubar['fontSize'];
	$fsp = $ubar['fontSizepx'] ?'true':'false';
	$kern = ($ubar['kerning']===false)? 'false': $ubar['kerning'];
	$prot = $ubar['protector'];
	$imgurl = "http://localhost/sig/single-line.php?mode=manager-test&font=$f&fontSize=$fs&fontSizepx=$fsp&kerning=$kern&protector=$prot";
?>
<html><head><title>Settings Manager for Sighature Generator with MAL plugin by Dvvarf</title>
<body>
Current script options:<br/>
Font: <?php echo $ubar['font']; ?><br/>
Font size: <?php echoFontSize($ubar); ?><br/>
Kerning: <?php echo ($ubar['kerning']===false)? 'disabled': $ubar['kerning']; ?><br/>
<img src="<?php echo $imgurl; ?>" />

<br/>Config to show:
<form method="get" action="#">
<input type="hidden" name="mode" value="manager" />
Font: <?php echoFontChanger($ubar); ?>
<br/>Kerning: <input type="text" name="kerning" value="<?php echo ($ubar['kerning']===false)? 'false': $ubar['kerning']; ?>" />
<br/>Font size: <input type="text" name="fontSize" value="<?php echo $ubar['fontSize']; ?>" /><?php echoFontSizeChanger($ubar); ?>
<br/>Protector: <input type="text" name="protector" value="<?php echo $ubar['protector']; ?>" />
<input type="submit" value="Show" />
</form>

</body></html>
<?php
die('');
}

	function echoFontSize($ubar) {
		echo $ubar['fontSize'].' ';
		if($ubar['fontSizepx']) {
				$i = 0; $h=0;
				$font_file = './fonts/'. $ubar['font'];
				while($h<$ubar['fontSize']) {
					$i++;
					$bbox = imagettfbbox($i, 0, $font_file, $ubar['text']);
					$h = abs($bbox[5] - $bbox[3]);
				}
				$fontSize = $i;
				echo "in pixels (<b>recommended value = $fontSize in points</b>)";
		} else echo 'in points (recommended)';
	}
	function echoFontSizeChanger($ubar) {
		echo "<select name=\"fontSizepx\">";
		if($ubar['fontSizepx']) {
			echo "<option value=\"true\">pixels</option><option value=\"false\">points (recommended)</option>";
		} else {
			echo "<option value=\"false\">points (recommended)</option><option value=\"true\">pixels</option>";
		}
		echo "</select>"
	}
	function echoFontChanger($ubar) {
		echo "<select name=\"font\"><option>{$ubar['font']}</option>";
		
		$fonts = folder_content('./fonts/','.ttf');
		array_splice($fonts,array_search($ubar['font'],$fonts),1); // removes the currently set font
		foreach($fonts as $fontfile) {
			echo "<option value=\"$fontfile\">$fontfile</option>";
		}
		
		echo "</select>"
	}
	// function returns contents of specified folder as array with extension filter
	function folder_content($dir_path='./',$filter='.php') {
		$dir = @opendir($dir_path);
		$content = array();
		while($read=readdir($dir)) if(strripos($read,$filter)!==false) $content[] = $read;
		closedir($dir); return $content;
	}
?>
<?php

class steam_mod {

function GetData($config,$get,$post) {
	// urls
	$id_url = 'http://steamcommunity.com/id/'.$config['steam_username'].'/?xml=1';
	$games_url = 'http://steamcommunity.com/id/'.$config['steam_username'].'/games?xml=1';
	
	// using connector to fetch xml data
	include_once('./connector.php');
	$conn = new connector();
	$xmlstr = $conn->fetchURLdata($id_url);
	if($xmlstr == false) return 'Connection failed';
	
	// parsing xml
	$xml_profile = new SimpleXMLElement($xmlstr);
	$played = (double)$xml_profile->hoursPlayed2Wk;
	
	if($played > 0.0) {
		if($played < 1.0) {
			$played = round($played*60);
			return 'Played '.$played.' mins for last 2 weeks';
		} else {
			return 'Played '.$played.' hrs for last 2 weeks';
		}
	} else {
		unset($xmlstr); unset($xml_profile); //possibly will save a byte of RAM for us + clean up everything from xmlstr
		$xmlstr1 = $conn->fetchURLdata($games_url);
		$xml_games = new SimpleXMLElement($xmlstr1);
		$max_played = 0;
		foreach($xml_games->games->game as $game) {
			if(isset($game->hoursOnRecord) && ((double)$game->hoursOnRecord > $max_played)) {
				$max_played = (double) $game->hoursOnRecord;
				$game_played = $game->name;
			}
		}
		return 'Most played - '.$game_played;
	}
}

}

?>
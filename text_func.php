<?php
	// Text functions for UB generator by Dvvarf
	define( 'TEMPLATE_TAG', '%' );

	function textlimit($string, $length=25, $english=false, $end="...") {
		return (my_strlen($string,$english)>$length ? trim( my_substr($string,0,$length-my_strlen($end,$english),$english) ).$end : $string);
	}

	function my_substr($str, $start=0, $length=1, $english=false, $sEncode="UTF-8", $oEncode="cp1251") {
		if($english) { return substr($str,$start,$length); } else {
			if (function_exists('mb_substr')) {
				return mb_substr($str,$start,$length,$sEncode);
			} else {
				$str = iconv($sEncode, $oEncode, $str);
				$tmp = ($length)?substr($str, $start, $length):substr($str, $start);
				$str = iconv($oEncode, $sEncode, $tmp);
				return $str;
			}
		}
	}

	function my_strlen($str, $english=false, $sEncode="UTF-8", $oEncode="cp1251") {
		if($english) { return strlen($str); } else {
			if (function_exists('mb_strlen')) {
				return mb_strlen($str,$sEncode);
			} else {
				return strlen(iconv($sEncode, $oEncode, $str));
			}
		}
	}

	function my_split($str) {
		preg_match_all('/./us', $str, $array);
		$a = array_shift($array);
		return $a;
	}

	function my_str_ireplace($search, $replace, $str, $english = false) {
		if($english) {
			return str_ireplace($search, $replace, $str);
		} else {
			foreach($search as $key => $entry)
				$search[$key] = '/'.$entry.'/iu';

			return preg_replace($search, $replace, $str);
		}
	}

	function GetPosX($size,$al='r7',$total=350) {
		$len = my_strlen($al);
		if($len>1) {
			$side = my_substr($al,0,1); // could be truncated to my_substr($al), but just in case...
			$last = my_substr($al,$len-1,1);
			$amount=($last=='%')?round(($size/100)*my_substr($al,1,$len-2)):my_substr($al,1,$len-1);
		} else {
			$side = $al; $amount = 0; $last = '';
		}

		switch($side) {
			case 'r': $pos = $total - ($size + $amount) - 1; break;
			case 'l': $pos = $amount; break;
			case 'c': $pos = ($total-$size)/2; break;
			default: $pos = $total - ($size+7) - 1; // coz default $al is r7
		}
		return $pos;
	}

	function GetPosY($size, $al='c', $total=19) {
		$len = my_strlen($al);
		if($len>1) {
			$side = my_substr($al,0,1); // could be truncated to my_substr($al), but just in case...
			$last = my_substr($al,$len-1,1);
			$amount=($last=='%')?round(($size/100)*my_substr($al,1,$len-2)):my_substr($al,1,$len-1);
		} else {
			$side = $al; $amount = 0; $last = '';
		}

		switch($side) {
			case 'b': $pos = $total - ($size + $amount); break;
			case 't': $pos = $amount; break;
			case 'c': $pos = ($total-$size)/2 - 1; break;
			default: $pos = (19-$size)/2 - 1; // coz default $al is c
		}
		return $pos;
	}

	function imagettfbboxf($size, $angle, $fontfile, $text) {
		// compute size with a zero angle
		$coords = imagettfbbox($size, 0, $fontfile, $text);
		// convert angle to radians
		$a = deg2rad($angle);
		// compute some usefull values
		$ca = cos($a);
		$sa = sin($a);
		$ret = array();
		// perform transformations
		for($i = 0; $i < 7; $i += 2){
			$ret[$i] = ceil($coords[$i] * $ca + $coords[$i+1] * $sa);
			$ret[$i+1] = floor($coords[$i+1] * $ca - $coords[$i] * $sa);
		}
		return $ret;
	}

	function distance($x1, $y1, $x2, $y2) {
		$x = $x1 - $x2;
		$y = $y1 - $y2;
		$dist = sqrt(pow($x, 2) + pow($y, 2));
		return sprintf('%0.1f', $dist);
	}

	// will return new point coords, that is standing at $kerning from x,y on $angle
	function applykerning($x, $y, $kerning, $angle) {
		$o['x'] = $x + $kerning * cos( deg2rad($angle) );
		$o['y'] = $y + $kerning * sin( deg2rad($angle) );
		return $o;
	}

	function ParseTemplate($template, $data) {
		$keys = array_keys($data);

		foreach($data as $key => $entry) {
			$tags[] = TEMPLATE_TAG .$key. TEMPLATE_TAG;
			$values[] = $entry;
		}

		return my_str_ireplace($tags, $values, $template);
	}
?>

<?php
/*
 * Connector class by Dvvarf
 * Can be used in any application, was created for UB Generator at first
 * PHP5, PHP4 suppoted in general (tested on 4.3; checkURLconnect is PHP5+ only)
 * 
 * Usage syntax:
 * 	$conn = new connector([$method = 'default'][$not_sure = false]); // creating object to connector, could be used to fetch many urls
 * 		// $method is optional parameter to make connector use certain method, not testing all of them one-by-one to determine first that will work; method will be used for all urls
 * 		// $not_sure is optional too: if $method is set, it will be prioritized higher than others methods in methods check, but other methods will have a chance in case first one fails
 * Examples of using the class:
 * 	$urlstr = $conn->fetchURLdata($url); // fetching $url and returning it as string $urlstr
 *  $conn->fetchURLdata($url, $filename, true, true); // will fetch the $url and directly write it to the $filename; will return true/false on success/fail
 * Methods currently supported:
 * 	'curl' - using CURL component; its the fastest one out there, but you should test if this could be used on your host
 * 	'socket' - using socket to recieve the data; its nearly as fast as CURL but requires socket opening to be enabled on your host (if safe-mode enabled - sockets are closed for sure)
 * 	'fread' - plain php file-handler reading; comes to the scene when others are not usable, but is not widely supported either (should not work in safe mode)
 * New methods addition:
 *	Your function must:
 *	1) get $url and $path as input parameters 
 *	2) return all the output from this $url as a string (in case $path === false) and file write status (in case $path !== false)
 *
 * And please share your function with me - if your method is good at some point (ultra-compatibleness, for example) i'll add it to this component.
 * 
 * Note on code styling:	@param* is just an optional @param
 */

/* Dont screw with these if you don't know what they are for */
define('HEADER_CHUNK_SIZE',	  512);
define('DATA_CHUNK_SIZE',	16384);

class connector {
	// public-like properties
	var $method			= 'default';
	var $cached			= false;
	var $useragent		= 'Mozilla/5.0 (Windows NT 5.1; rv:21.0) Gecko/20100101 Firefox/21.0';
	var $userlang		= 'en-us';

	// private-like properties (still can be used outside in some cases)
	var $methods		= array('curl', 'socket', 'fread');
	var $nmethods		= array();
	var $compression	= 'gzip, deflate'; // supported compression methods, set to '' to disable (not recommended)

	// constraints:
	var $max_redirects	= 3;	// sets maximum number of redirects to follow
	var $max_retries	= 5;	// max tries to download the whole file

	// for internal use:
	var $redirected		= 0; 	// how many redirects was handled to the time
	var $retries		= 0;	// how many times we tried to get the whole file
	var $expected_size	= 0;	// expected size of file being downloaded
	var $resume_offset	= 0;	// byte-offset to resume from
	var $errorTriggered = false;// changed to 'true' if any connection method thrown exception of some kind

	// headers:
	var $headers		= array();

	// for debugging purposes:
	var $internals		= null;	// stores all the previous values of the above variables after doReset function

/*
 * Class constructor
 *
 * Sets $method to use in class
 * Sets error handler to get rid of possible errors and warnings
 *
 * @param*	string	$method		method to use to grab links ('curl'/'socket'/'fread')
 * @param*	boolean	$not_sure	true: will check all supported method, $method will just be the first of them; false: only $method will be used (and error aill be returned if failed)
 *
 * @return						constructor do not return anything, right?
 *
 */

function connector($method = 'default', $not_sure = false) {
	// method from parameters supported by class?
	$exists = array_search($method, $this->methods);

	if($exists && !$not_sure) {
		$this->method = $method;
	} elseif($exists && $not_sure) {
		array_splice($this->methods, array_search($method, $this->methods), 1);
		array_unshift($this->methods,$method);
		$this->method = 'default';
	} else {
		$this->method = 'default';
	}
	
	// setting up additional headers
	if($this->compression !== '')
		$headers['Accept-Encoding'] = 'Accept-Encoding: '. $this->compression;

	if($this->userlang !== '')
		$headers['Accept-Language'] = 'Accept-Language: '. $this->userlang;

	set_error_handler(array('connector', 'MyErrorHandler'));
}

/*
 * Function to retrieve the data from url using CURL
 *
 * All these functions (get_data_curl, get_data_socket, get_data_fread) are implementing
 *  different methods of retrieving the data (source codes of some pages, in general case);
 *  all of them got the same params ($url) and return variables of the same (string) type.
 *
 * @param	string	$url	url from where to grab the data
 * @param*	string	$path	if set (not false) - direct writing to output file is used, otherwise works as intended
 *
 * @return	string			string with recieved data
 * @return	boolean			true if function completed succesfully (write to $path file succeeded) or false if error was triggered (reading from $url or writing into $path failed)
 *
 */

function get_data_curl($url, $path = false) {
	if(!function_exists('curl_init')) return false;
	$ch = curl_init();
	
	curl_setopt ($ch, CURLOPT_URL, $url);
	if(isset($this->headers['Accept-Encoding'])) {
		curl_setopt ($ch, CURLOPT_ENCODING, $this->compression);
		unset($this->headers['Accept-Encoding']);
	}
	curl_setopt ($ch, CURLOPT_HTTPHEADER, $this->headers);
	curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt ($ch, CURLOPT_AUTOREFERER, TRUE);
	curl_setopt ($ch, CURLOPT_MAXREDIRS, $this->max_redirects);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt ($ch, CURLOPT_USERAGENT, $this->useragent);
	
	if($this->resume_offset > 0)
		curl_setopt ($ch, CURLOPT_RESUME_FROM, $this->resume_offset);
	
	if($path === false) {
		// return the result as a string
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$curl_string = curl_exec($ch);
		curl_close($ch);
		return $curl_string;
	} else {
		// writing directly - need to return status, not content
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, FALSE);
		$f = fopen($path, 'w');
		
		curl_setopt ($ch, CURLOPT_FILE, $f);
		$status = curl_exec($ch);
		$size = curl_getinfo ($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
		curl_close($ch);
		
		if($size > 0) {
			// only set expected_size if not resuming (or else we will just end with wrong size)
			if($this->resume_offset <= 0) $this->expected_size = $size;
			$result_size = filesize($path);
			if($status && ($this->expected_size > $result_size)) {
				// count 'retries'
				$this->retries = $this->retries + 1;
				if($this->retries >= $this->max_retries) {
					fclose($f);
					$this->doReset();
					return false;
				} else {
					$this->resume_offset = $result_size;
					return $this->get_data_curl($url, $path);
				}
			}
		}
		
		fclose($f);
		$this->doReset();
		return $status;
	}
}

/*
 * Function to retrieve the data from url using sockets (the most advanced method to the day)
 *
 * @param	string		$url	url from where to grab the data
 * @param*	string		$path	if set (not false) - direct writing to output file is used, otherwise works as intended
 *
 * @return	string				string with recieved data
 * @return	boolean				true if function completed succesfully (write to $path file succeeded) or false if error was triggered (reading from $url or writing into $path failed)
 *
 */

function get_data_socket($url, $path = false) {
	$url_array = parse_url($url);

	$fp = fsockopen($url_array['host'], (isset($url_array['port'])?$url_array['port']:80), $errno, $errstr, 5);

	if($this->errorTriggered) {
		$this->errorTriggered = false;
		return false;
	}
	if($errno!=0) return false;
	
	if($url_array['query'] == '') {
		$send = "GET " . $url_array['path'] ." HTTP/1.1\r\n";
	} else {
		$send = "GET " . $url_array['path'] . "?" . $url_array['query'] ." HTTP/1.1\r\n";
	}
	
	if($this->resume_offset > 0) $send .= "Range: bytes=". $this->resume_offset ."-\r\n";
	$send .= "User-Agent: ". $this->useragent ."\r\n";
	$send .= "Host: " . $url_array['host'] . "\r\n";
	foreach($this->headers as $header)
		$send .= $header ."\r\n";
	$send .= "Connection: Close\r\n\r\n";
	fwrite($fp, $send); // writing the headers to socket
	
	// preparing variables
	$location = ''; $size = 0;
	// transfer params (defined later by recieved headers)
	$resumable = false;
	$chunked = false;
	$gzipped = false;
	$header_end = false; $data = '';
	
	// we should handle the redirect properly
	// props to this comment on php.net for the idea and portions of code:
	// http://www.php.net/manual/en/function.get-headers.php#84957
	while($fp && !$header_end && !feof($fp)) {
		$buffer = fgets($fp, HEADER_CHUNK_SIZE);
		if(urlencode($buffer) == "%0D%0A") {
			$header_end = true;
		} else {
			$buffer = rtrim($buffer);
			list($key, $value) = explode(': ', $buffer, 2);
			
			switch($key) {
				case "$buffer":
					if((strpos($buffer, '301')>0)||(strpos($buffer, '302')>0)||(strpos($buffer, '303')>0)) $redirect = true;
					break;
				case 'Location': // we're being redirected
					$location = $value;
					break;
				case 'Content-Length': // size of the recieved file in bytes (optional)
					if($this->resume_offset <= 0) $size = $value;
					break;
				/*case 'Content-Range':
					$resumed = true;
					break;*/
				case 'Accept-Ranges':
					if($value == 'bytes') $resumable = true;
					break;
				case 'Content-Encoding':
					if($value == 'gzip') $gzipped = true;
					break;
				case 'Transfer-Encoding':
					$chunked = true;
					break;
			}
		}
	}
	
	// handling redirects
	if($redirect && ($location !== '')) {
		// properly closing previously opened socket
		fclose($fp);
		
		// writing redirects count to not end into infinite recursion
		$this->redirected = $this->redirected + 1;
		if($this->redirected >= $this->max_redirects) {
			// if we are going to reuse the object we should clear the 'redirected' count
			$this->doReset();
			return false;
		} else {
			// recursively calling the function
			return $this->get_data_socket($location, $path);
		}
	} elseif($redirect) return false; // redirect leads us to nowhere
	
	// returning a string
	if($path === false) {
		if($header_end)
			while ($fp && !feof($fp))
				$data .= $this->load_chunk($fp, $chunked, $gzipped);
	} else {
		// direct writing
		
		// decide to write file anew or append the new part to it
		$handle = ($this->resume_offset > 0) ? fopen($path, 'a') : fopen($path, 'w');
		
		if(!$handle) return false;
		
		if($header_end)
			while ($fp && !feof($fp)) {
				$buffer = $this->load_chunk($fp, $chunked, $gzipped);
				if(fwrite($handle, $buffer)===false) return false;
			}
		
		fclose($handle);
		if($size > 0) {
			// only set expected_size if not resuming (or else we will just end with wrong size)
			if($this->resume_offset <= 0) $this->expected_size = $size;
			$result_size = filesize($path);
			if(($this->expected_size > $result_size) && $resumable) {
				$this->retries = $this->retries + 1;
				if($this->retries >= $this->max_retries) {
					fclose($fp);
					$this->doReset();
					return false;
				} else {
					$this->resume_offset = $result_size;
					return $this->get_data_socket($url, $path);
				}
			} elseif(!$resumable) {
				$this->doReset();
				return false;
			}
		}
	}
	
	fclose($fp);
	$this->doReset();
	return ($path === false) ? $data : true;
}

/*
 * Function to retrieve the data from url using fread
 *
 * @param	string	$url	url from where to grab the data
 * @param*	string	$path	if set (not false) - direct writing to output file is used, otherwise works as intended
 *
 * @return	string			string with recieved data
 * @return	boolean			true if function completed succesfully (write to $path file succeeded) or false if error was triggered (reading from $url or writing into $path failed)
 *
 */

function get_data_fread($url, $path = false) {
	if(!ini_get('allow_url_fopen')) return false;
	ini_set('user_agent', $this->useragent );

	$remote = fopen("$url","r");

	if($this->errorTriggered) {
		$this->errorTriggered = false;
		return false;
	}
	
	if(!$remote) return false;
	
	if($path === false) {
		$buffer = '';
		while ( ($buf=fread( $remote, DATA_CHUNK_SIZE )) !== '' ) $buffer .= $buf;
	} else {
		$local = fopen($path, 'w');
		if(!$local) return false;
		
		$size = 0; $resumable = false;
		$meta = stream_get_meta_data($remote);
		foreach($meta['wrapper_data'] as $buffer) {
			list($key, $value) = explode(': ', $buffer, 2);
			
			// when handling redirects fread stacks all the headers from different pages in one array
			// anyway after this $size will containg the size of last page/file
			if($key == 'Content-Length') {
				$size = $value;
			} elseif($key == 'Accept-Ranges') {
				if($value == 'bytes') $resumable = true;
			}
		}
		
		while ( ($buf=fread( $remote, DATA_CHUNK_SIZE )) !== '' )
			if(fwrite($local, $buf)===false) return false;
		
		fclose($local);
		
		if($size > 0) {
			// only set expected_size if not resuming (or else we will just end with wrong size)
			if($this->resume_offset <= 0) $this->expected_size = $size;
			$result_size = filesize($path);
			if(($this->expected_size > $result_size) && $resumable) {
				$this->retries = $this->retries + 1;
				if($this->retries >= $this->max_retries) {
					fclose($fp);
					//$this->retries = 0;
					$this->doReset();
					return false;
				} else {
					$this->resume_offset = $result_size;
					return $this->get_data_fread($url, $path);
				}
			} elseif(!$resumable) {
				$this->doReset();
				return false;
			}
		}
	}
	
	$this->doReset();
	fclose($remote);
	return ($path === false) ? $buffer : true;
}

/*
 * General function to fetch the data
 *
 * Tries all of the functions above in order to get one of theme working or
 *  uses the one method provided via parameter.
 *
 * @param	string		$url			url from where to grab the data
 * @param*	string		$pth			path where to save recieved data (default = false)
 * @param*	boolean		$bool_error		if set to false, then function returns some text on fail (default = true)
 * @param*	string		$direct			if set (not false) - direct writing to output file is used, otherwise works as intended
 *
 * @return	boolean						returns false only if($bool_error) and error really happened while processing the function
 * @return	string						string with recieved data or text reporting about the error on fail
 *
 * Comments on local variables:
 * 	$a - usually contains data returned by one of methods
 * 	$b - data returned by fwritenclose (boolean stricly)
 *
 */

function fetchURLdata($url, $pth = false, $bool_error = true, $direct = false) {
	if((!is_bool($pth)) && ($direct === true)) $direct = $pth;
	if($this->method == 'default') {
		$used_method = reset($this->methods);
		$a = $this->use_method($url,$used_method,$direct);
		if(!$a) {
			// method failed: we are going to remove it from array of pending methods and then call the function again to try another one
			$this->nmethods = array_merge($this->nmethods,array_splice($this->methods,0,1));
			if(count($this->methods) == 0) {
				return ($bool_error)?false:'Failed to fetch data - not a single method worked.';
			} else {
				return $this->fetchURLdata($url,$pth,$bool_error,$direct);
			}
		} elseif($a && $pth === true) {
			// method worked (and should be cached): we just store the values and return the result
			$this->method = $used_method;
			$this->cached = $a;
			return $a;
		} elseif($a && $pth) {
			// method worked (and should be written to file)
			$this->method = $used_method;
			// if we used direct writing, then we got nothing to write, else we should write the result and then return it
			$b = ($direct === false)?$this->fwritenclose($pth,$a):true;
			if($b) return $a; else return ($bool_error) ? false: 'Failed to write to specified path.';
		} elseif($a && !$pth) {
			// method worked (default case - no file output)
			$this->method = $used_method;
			return $a;
		}
	} else {
		// we got particuliar method set to work with
		$a = $this->use_method($url,$this->method,$direct);
		if($a && $pth === true) {
			// method worked (and cached)
			$this->cached = $a;
		} elseif($a && $pth) {
			// method worked (and was written to file - directly or not)
			$b = ($direct === false)?$this->fwritenclose($pth,$a):true;
			if($b) return $a; else return ($bool_error)?false:'Failed to write to specified path.';
		}
		return $a;
	}
}

/*
 * This function is just wrapper for multiple data retrievers (just calls corresponding function for given $method)
 *
 * @param	string		$url			url from where to grab the data
 * @param*	string		$method			method to be called
 * @param*	string		$direct			if set (not false) - direct writing to output file is used, otherwise works as intended
 *
 * @return	boolean						false if there is no function for $method
 * @return	mixed						returns the output of called method
 *
 */

function use_method($url, $method, $direct = false) {
	switch($method) {
		case 'curl': return $this->get_data_curl($url,$direct); break;
		case 'socket': return $this->get_data_socket($url,$direct); break;
		case 'fread': return $this->get_data_fread($url,$direct); break;
	}
	return false;
}

/*
 * This function will return boolean if connection with $url was successfull using sockets. PHP5+ only function
 *
 * @param	string		$url	url to try to connect to
 *
 * @return	boolean				boolean being returned: true - if $url connectible, false - if not
 *
 */

function checkURLconnect($url) {
	$url_array = parse_url($url);

	if( (isset($url_array['scheme'])) && (in_array($url_array['scheme'],stream_get_transports())) ) {
		$scheme = $url_array['scheme'].'://';
	} else {
		// maybe we should set this to tcp by default?
		$scheme = '';
	}

	$fp = fsockopen($scheme . $url_array['host'], (isset($url_array['port'])?$url_array['port']:80), $errno, $errstr, 5);
	
	if($this->errorTriggered) {
		$this->errorTriggered = false;
		return false;
	}
	
	return ($fp==false)?false:true;
}

/*
 * This function will write cached data into certain file. Function just calls fwritenclose on $this->cached.
 *
 * @param	string		$filename	path where to save the data
 *
 * @return	boolean					returns true if all went well else false
 *
 */

function writeCachedResult($filename) {
	if($this->cached) {
		return $this->fwritenclose($filename,$this->cached);
	} else {
		return false;
	}
}

/*
 * Plain function to open/create the file, write something to it and then close the file
 *
 * @param	string		$filename	path where to save the data
 * @param	string		$content	string to write to a file, specified with $filename
 *
 * @return	boolean					returns true if all went well else false
 *
 */

function fwritenclose($filename, $content) {
	if(!($handle = fopen($filename, 'w')))	return false;
	if(fwrite($handle,$content)===false) 	return false;
	fclose($handle);
	return true;
}

/*
 * Function is zeroing all the internal variables and writes their previous values in array (for debugging purposes)
 *
 * @param*	boolean		$return		set to true if you want previous values to be returned
 *
 * @return	boolean					always returns true
 *
 */

function doReset($return = false) {
	$this->internals		= array(
		$this->redirected, $this->retries, $this->expected_size, $this->resume_offset
	);
	$this->redirected		=  0;
	$this->retries			=  0;
	$this->expected_size	=  0;
	$this->resume_offset	=  0;
	return ($return) ? $this->internals : true;
}

/*
 * Loads data chunk by chunk from $fp
 *
 * Courtesy of vitall @ http://www.php.net/manual/en/function.gzinflate.php#93435
 *
 * @param	string		$d			string to decode data from
 * @param	boolean		$gzipped	data is gzipped (http header 'Content-Encoding')
 * @param	boolean		$chunked	data is chunked (http header 'Transfer-Ecoding')
 *
 * @return	boolean					always do return true
 *
 */

function load_chunk($fp, $chunked = false, $gzipped = false) {
	if(!$fp)
		return false;
	
	if($chunked) {
		// setting temp variables
		$length = DATA_CHUNK_SIZE;
		$lrn = strlen("\r\n");
		$buffer = ''; $str = ''; $fl = true;
		
		// reading chunk by chunk
		do {
			$str = fgets($fp, $length);		// reading a string
			$length = hexdec($str) + $lrn;	// decoding it (to get chunk size)
			if($length > $lrn) {			// if chunk is big enough
				do {
					$str = fgets($fp, $length);	// get part of chunk
					$buffer .= $str;			// add chunk to output string
					$length -= strlen($str);	// we should now decrease the $length for the next chunk part
					$str = '';
				} while($length > $lrn);		// chunk ended?
			} else {
				$fl = false;					// chunk ended
			}
		} while($fl);
		
		// decoding gzipped chunk
		return ($gzipped) ? gzinflate(substr($buffer,10)) : $buffer;
	} else {
		// decoding gzipped string (got no chunks)
		return ($gzipped) ? gzinflate(substr(fgets($fp, DATA_CHUNK_SIZE),10)) : fgets($fp, DATA_CHUNK_SIZE);
	}
}

/*
 * Triggers errorTriggered variable on exception
 *
 * This function is used to test all the functions without rising any warnings or errors. Please, dont touch it. =)
 *
 * @param	string		$errno		number for exception raised
 * @param	string		$errstr		string, describing the error occured
 *
 * @return	boolean					always do return true
 *
 */

function MyErrorHandler($errno, $errstr) {
	$connector->errorTriggered = true;
	return true;
}

}
?>
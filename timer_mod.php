<?php
// Timer module for Signature Generator by Dvvarf v0.2

class timer_mod {

function GetData($config,$get=0,$post=0) {
	include_once('./text_func.php');
	
	$odate = strtotime($config['timer']); // normal date to unix timestamp
	$cdate = time(); // current date (time() output may be changed while script is running, so lets save it to variable)
	$text = (isset($config['timer_prefix']))?$config['timer_prefix']:'';
	
	$milestones = 'y,mn,w,d,h,m,s';
	if(isset($config['timer_min']) && stripos($milestones,$config['timer_min'])) {
		$milestones = my_substr($milestones,0,strripos($milestones,$config['timer_min'])+my_strlen($config['timer_min']));
	} else {
		$milestones = 'y,mn,w,d';
	}
	if(isset($config['timer_max']) && stripos($milestones,$config['timer_max'])) {
		$timer_max = stripos($milestones,$config['timer_max']);
		$milestones = my_substr($milestones,$timer_max,strlen($milestones)-$timer_max);
		$elapsed = $this->calc_tl($odate,$cdate,$config['timer_max']);
	} else {
		$elapsed = $this->calc_tl($odate,$cdate);
	}
	
	$stones = explode(',',$milestones);
	
	//$elapsed = calc_tl(strtotime("01 February 2009 12:00am"),time());
	// Для простоты в массиве array(1 год, 2 года, 5 лет)
	$loc = array(
		'y' 	=> array('год','года','лет'),
		'mn' 	=> array('месяц','месяца','месяцев'),
		'w' 	=> array('неделя','недели','недель'),
		'd' 	=> array('день','дня','дней'),
		'h' 	=> array('час','часа','часов'),
		'm' 	=> array('минута','минуты','минут'),
		's' 	=> array('секунды','секунды','секунд')
	);
	if($odate < $cdate) { // если дата, указанная для таймера уже прошла
		$loc['w'] = array('неделю','недели','недель');
		$loc['m'] = array('минуту','минуты','минут');
		$loc['s'] = array('секунду','секунды','секунд');
	}
	
	foreach($stones as $stone) {
		$text .= $this->print_string_piece($stone,$elapsed,$loc);
	}

	$text .= (isset($config['timer_postfix']))?$config['timer_postfix']:'';
	return $text;
}

function print_string_piece($key,$elapsed,$loc) {
	if ($elapsed[$key]>0) {
		return ' '.$elapsed[$key].' '.$this->declOfNum($elapsed[$key],$loc[$key]);
	}
}

function calc_tl($t, $sT = 0, $max = 'Y') {

	$sY = 31536000;
	$sMn= 2628000;
	$sW = 604800;
	$sD = 86400;
	$sH = 3600;
	$sM = 60;
	
	if($sT) $t = ($sT - $t);
//	if($t <= 0) $t = 0;
	$t = abs($t);
	$r['string'] = '';
	
	$bs[1] = ('1'^'9'); /* Backspace */
	
	switch (strtolower($max)) {
		case 'y':
				$y = ((int)($t / $sY));
				$t = ($t - ($y * $sY));
//				$r['string'] .= "{$y} years{$bs[$y]} ";
				$r['y'] = $y;
		case 'mn':
				$mn = ((int)($t / $sMn));
				$t = ($t - ($mn * $sMn));
//				$r['string'] .= "{$mn} month{$bs[$mn]} ";
				$r['mn'] = $mn;
		case 'w':
				$w = ((int)($t / $sW));
				$t = ($t - ($w * $sW));
//				$r['string'] .= "{$w} weeks{$bs[$w]} ";
				$r['w'] = $w;
		case 'd':
				$d = ((int)($t / $sD));
				$t = ($t - ($d * $sD));
//				$r['string'] .= "{$d} days{$bs[$d]} ";
				$r['d'] = $d;
		case 'h':
				$h = ((int)($t / $sH));
				$t = ($t - ($h * $sH));
//				$r['string'] .= "{$h} hours{$bs[$h]} ";
				$r['h'] = $h;
		case 'm':
				$m = ((int)($t / $sM));
				$t = ($t - ($m * $sM));
//				$r['string'] .= "{$m} minutes{$bs[$m]} ";
				$r['m'] = $m;
		case 's':
				$s = $t;
//				$r['string'] .= "{$s} seconds{$bs[$s]} ";
				$r['s'] = $s;
				break;
		default:
				return $this->calc_tl($t);
		break;
	}

	return $r;
}

function declOfNum($number, $titles) {
	$cases = array (2, 0, 1, 1, 1, 2);
	return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
}

}

?>